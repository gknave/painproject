import numpy as np
from scipy import optimize
import utils.gaussians as gs
import matplotlib.pyplot as plt

def aic(data, params, ll=gs.maxnormal_ll):
    n_params = len(params)
    return 2*ll(params, data=data) + 2*n_params

def bic(data, params, ll=gs.maxnormal_ll):
    n_samples = data.shape[0]
    n_params = len(params)
    return 2*ll(params, data=data) + np.log(n_samples)*n_params

if __name__=='__main__':
    gap = 10
    mydata = np.concatenate((np.random.normal(loc=0, size=(100,)), 
                             np.random.normal(loc=gap, size=(100,))))
    llfunc = gs.gaussmixture_ll
    name = 'gaussmixture'
    clusterlist = [1, 2, 3, 4, 5, 6]
    aicvals = np.zeros(len(clusterlist))
    bicvals = np.zeros(len(clusterlist))
    for k, c in enumerate(clusterlist):
        if llfunc == gs.maxnormal_ll:
            guess = []
        elif llfunc == gs.gaussmixture_ll:
            guess = [1/c for n in range(c-1)]
        for n in range(c):
            guess += [n-0.5, 0.5]
        res = optimize.minimize(llfunc, guess, args=(mydata,), method='Nelder-Mead')
        aicvals[k] = aic(mydata, res.x, ll=llfunc)
        bicvals[k] = bic(mydata, res.x, ll=llfunc)
    fig, ax = plt.subplots(figsize=(3,2))
    ax.plot(clusterlist, aicvals, '-o')
    ax.plot(clusterlist, bicvals, '-o')
    ax.set_ylabel('AIC, BIC')
    ax.set_xlabel('Num. clusters')
    ax.set_xticks(clusterlist)
    fig.savefig(f'../Figures/{name}AIC_{gap}.eps', bbox_inches='tight', transparent=True)
    fig.savefig(f'../Figures/{name}AIC_{gap}.png', dpi=300, bbox_inches='tight', transparent=True)
    plt.show()
