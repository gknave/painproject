import numpy as np
from scipy import io, interpolate
from sklearn import cluster
import pandas as pd

wlen = 14
wcutoff = 5

def load_data():
    datapath = '../Data'
    tmpx = io.loadmat(f'{datapath}/Clifton/PatientData_final_withheaders.mat')
    headers=[x[0] for x in tmpx['column_headers'][0]]
    patient_list=[x for x in tmpx.keys() if 'Patient' in x]
    for patient in patient_list:
        tmp=tmpx[patient]
        df=pd.DataFrame(tmp,columns=headers)
        df['Patient_ID']=patient
        collist=[df.columns[-1]]+list(df.columns[:-1])
        df=df[collist]
        if patient==patient_list[0]:
            patient_df=df.copy()
        else:
            patient_df=patient_df.append(df)
    return patient_df, patient_list

def make_dicts():
    patient_df, patient_list = load_data()
    tdict = {}
    paindict = {}
    for patient in patient_list:
        inds = np.where(np.logical_and(patient_df['Patient_ID']==patient, ~patient_df['VasPain'].isnull()))[0]
        time = patient_df['days'].iloc[inds]
        pain = patient_df['VasPain'].iloc[inds]
        tdict[patient] = time.values
        paindict[patient] = pain.values
    return tdict, paindict

def window_data():
    tdict, paindict = make_dicts()
    window_df = pd.DataFrame(columns=['Group', 'Patient', 'Time', 'Pain', 't0'])
    group = 0
    for key in tdict.keys():
        t = tdict[key]
        pain = paindict[key]
        for tmax in np.arange(wlen, np.ceil(t[-1]/wlen)*wlen, wlen):
            tmin = tmax-wlen
            inds = np.where(np.logical_and(t>tmin, t<tmax))[0]
            datadict = {
                'Group':[group for _ in inds],
                'Patient':[key for _ in inds],
                'Time':t[inds]-tmin,
                'Pain':pain[inds],
                't0':tmin
                }
            if len(inds) > wcutoff:
                temp_df = pd.DataFrame(data=datadict, columns=['Group', 'Patient', 'Time', 'Pain', 't0'])
                window_df = pd.concat([window_df, temp_df], axis='index')
                group += 1
    return window_df

def group_windowed():
    _, patient_list = load_data()
    window_df = window_data()
    groups = int(np.max(window_df['Group'].values)+1)
    ptnum = {}
    for j, patient in enumerate(sorted(patient_list)):
        ptnum[patient] = j
    windowed = np.zeros((groups, 6))
    for a, g in window_df.groupby('Group'):
        pain = g['Pain']
        windowed[a, 0] = np.mean(pain)/10
        windowed[a, 1] = np.std(pain)/5
        windowed[a, 2] = len(np.where(pain<=3)[0])/len(pain)
        windowed[a, 3] = len(np.where(pain>=8)[0])/len(pain)
        windowed[a, 4] = np.mean(np.gradient(pain))
        windowed[a, 5] = ptnum[g['Patient'][0]]
    return windowed

def centered_hist_windowed():
    _, patient_list = load_data()
    window_df = window_data()
    groups = int(np.max(window_df['Group'].values)+1)
    ptnum = {}
    for j, patient in enumerate(sorted(patient_list)):
        ptnum[patient] = j
    cent_hist = np.zeros((groups, 23))
    for a, g in window_df.groupby('Group'):
        pain = g['Pain']
        pmin = np.min(pain)
        pmax = np.max(pain)
        pain -= np.mean(pain)
        pain /= np.std(pain)
        cent_hist[a, :-3], _ = np.histogram(pain, bins=20, range=(-4, 4), density=False)
        cent_hist[a, -3] = pmin
        cent_hist[a, -2] = pmax
        cent_hist[a, -1] = ptnum[g['Patient'][0]]
    return cent_hist

def spline_windowed(t_steps=672):
    _, patient_list = load_data()
    window_df = window_data()
    groups = int(np.max(window_df['Group'].values)+1)
    ptnum = {}
    for j, patient in enumerate(sorted(patient_list)):
        ptnum[patient] = j
    spline_data = np.zeros((groups, t_steps+1))
    t_values = np.linspace(0, 14, t_steps)
    for a, g in window_df.groupby('Group'):
        time = g['Time']
        pain = g['Pain']
        spl = interpolate.interp1d(time, pain, bounds_error=False, kind='nearest', fill_value='extrapolate', assume_sorted=True)
        spline_data[a, :-1] = spl(t_values)
        spline_data[a, -1] = ptnum[g['Patient'][0]]
    return spline_data

def synthetic_paindata(cluster_means=[2, 8], noise=1, clustersize=100):
    time = {}
    pain = {}
    for i, mean in enumerate(cluster_means):
        for j in range(clustersize):
            key = f'c{i}_{j}'
            samples = int(np.round(np.random.uniform(low=5, high=21)))
            time[key] = np.sort(np.random.uniform(size=(samples,))*14)
            pain_data = np.random.normal(loc=mean, scale=noise, size=(samples,))
            pain_data[pain_data < 0] = 0
            pain_data[pain_data > 10] = 10
            pain[key] = pain_data
    return time, pain

def interpolated_synth(t_steps=672):
    time, pain = synthetic_paindata()
    spline_data = np.zeros((len(time.keys()), t_steps))
    t_values = np.linspace(0, 14, t_steps)
    for a, key in enumerate(sorted(time.keys())):
        t = time[key]
        p = pain[key]
        spl = interpolate.interp1d(t, p, bounds_error=False, kind='nearest', fill_value='extrapolate', assume_sorted=True)
        spline_data[a, :] = spl(t_values)
    return spline_data

### Functions to check clustering validity
def rss(data, cc, labels):
    error = 0
    for (xy, l) in zip(data, labels):
        d = xy-cc[l, :]
        error += np.dot(d, d)
    return error

def aic(data, labels):
    _, M = data.shape()
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = np.where(labels==l)[0]
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    return rss(data, cc, labels) + 2*K*M

def bic(data, labels):
    N, M = data.shape()
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = np.where(labels==l)[0]
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    return rss(data, cc, labels) + np.log(N)*K*M

def relabel(data, labels):
    labels = labels.astype(int)
    N = int(np.max(labels)+1)
    midlist = np.zeros((N,))
    for c in range(N):
        clist = np.where(labels == c)[0]
        midlist[c] = np.mean(data[clist, 0].ravel())
    pairs = list(enumerate(midlist))
    sortlabels = sorted(pairs, key=lambda x: x[1])
    relabels = np.zeros(labels.shape)
    for new_l, (l, _) in enumerate(sortlabels):
        relabels = np.where(labels==l, new_l, relabels)
    return relabels

##### Functions used for assessing the co-occurrence matrix
def adj_from_labels(all_labels):
    num_samples = all_labels.shape[1]
    num_trials = all_labels.shape[0]
    adjacency = np.ones((num_samples, num_samples))
    for i in range(num_samples):
        for j in range(i+1, num_samples):
            weight = np.sum(all_labels[:, i]==all_labels[:, j])/num_trials
            adjacency[i, j] = weight
            adjacency[j, i] = weight
    return adjacency

def matrix_kmeans(adjacency, n_clusters=2):
    kmeans = cluster.KMeans(n_clusters=n_clusters).fit(adjacency)
    labels = kmeans.labels_
    communities = [list(np.argwhere(labels==j).ravel()) for j in np.unique(labels)]
    return communities

def reorder_matrix(adjacency, communities):
    size = adjacency.shape[0]
    reindex = []
    for com in communities:
        reindex += com
    reordered = np.ones((size, size))
    for i in range(size):
        for j in range(i+1, size):
            m = reindex[i]
            n = reindex[j]
            reordered[i, j] = adjacency[m, n]
            reordered[j, i] = adjacency[m, n]
    return reordered

def blockdiag(communities):
    comlens = [len(com) for com in communities]
    out = np.zeros((np.sum(comlens), np.sum(comlens)))
    start = 0
    for length in comlens:
        out[start:start+length, start:start+length] = 1
        start += length
    return out

def all_errors(adjacency, communities, norm=1):
    reordered = reorder_matrix(adjacency, communities)
    ideal = blockdiag(communities)
    return (np.abs(ideal-reordered)**norm)**(1/norm)

def modularity(adjacency, communities):
    k = np.sum(adjacency, axis=-1)
    m = np.sum(k)/2
    Q = 0
    for com in communities:
        for c in com:
            for d in com:
                B = adjacency[c, d]-k[c]*k[d]/(2*m)
                Q += B/(2*m)
    return Q