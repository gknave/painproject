import os
import pickle5 as pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from utils import matrix

# Keys: group_key (group, original_group), affinity_matrix, n_clusters, labels
def get_data(file):
    with open(file, 'rb') as input:
        data = pickle.load(input)
    return data 

def save_data(data, file):
    with open(file, 'wb') as fp:
            pickle.dump(data, fp, protocol=pickle.HIGHEST_PROTOCOL)

datapath = 'Data/resamples'
figpath = 'Figures/resamples'
if not os.path.isdir(figpath):
    os.mkdir(figpath)

sluglist = ['nninterp', 'nninterp_synth', 'lininterp', 'lininterp_synth', 'histogram', 'histogram_synth']
# sluglist = ['lininterp_synth', ]
for slug in sluglist:
    loaddir = f'{datapath}/{slug}'
    figdir = f'{figpath}/{slug}'
    if not os.path.isdir(figdir):
        os.mkdir(figdir)

def random_probability(communities, datalen):
    grouplens = np.zeros((len(communities),))
    for i, c in enumerate(communities):
        grouplens[i] = len(c)
    return np.sum((grouplens/datalen)**2)
    
def maximum_error(communities, datalen):
    p = random_probability(communities, datalen)
    return np.sqrt(p*(1-p))

if __name__=='__main__':
    for slug in sluglist:
        loaddir = f'{datapath}/{slug}'
        figdir = f'{figpath}/{slug}'
       # Initiate datalen, occurrence, cooccurrence 
        temp = get_data(f'{loaddir}/test0.pkl')
        datalen = temp['affinity_matrix'].shape[0]    
        occurrence = np.zeros((datalen, datalen))
        cooccurrence = np.zeros((datalen, datalen, len(temp['n_clusters'])))
        freqout = np.zeros(cooccurrence.shape)
        labeloutput = np.zeros((datalen, len(temp['n_clusters'])))
        del(temp)
        for file in os.listdir(loaddir):
            if 'finallabels' in file:
                continue
            data = get_data(f'{loaddir}/{file}')
            alllabels = np.hstack((np.array(data['group_key']), data['labels']))
            grouplist = np.unique(alllabels[:, 1])
            grouplist = [int(g) for g in grouplist]
            for g in grouplist:
                occurrence[g, grouplist] += 1
            for n in data['n_clusters']:
                for label in np.unique(alllabels[:, n]):
                    inds = np.where(alllabels[:, n]==label)[0]
                    groups = [int(g) for g in np.unique(alllabels[inds, 1])]
                    for g in groups:
                        cooccurrence[g, groups, int(n)-2] += 1
        cost_values = np.zeros((len(data['n_clusters'],)))
        max_errors = np.zeros(cost_values.shape)
        robustness = np.zeros((len(data['n_clusters'],)))
        Q_values = np.zeros((len(data['n_clusters'],)))
        print(slug)
        for n in data['n_clusters']:
            frequency = cooccurrence[:, :, int(n)-2]/occurrence
            freqout[:, :, int(n)-2] = frequency
            communities = matrix.matrix_kmeans(frequency, n_clusters=n)
            labeloutput[:, int(n)-2] = matrix.labels(communities)
            # Generate output metrics
            cost_values[int(n)-2] = matrix.mean_error(frequency, communities, norm=2)
            Q_values[int(n)-2] = matrix.modularity(frequency, communities)
    #         max_err = np.sqrt(n-1)/n
            max_err = maximum_error(communities, datalen)
            max_errors[int(n)-2] = max_err
            robustness[int(n)-2] = 1 - cost_values[int(n)-2]/max_err
            # belongingness
            reordered = matrix.reorder_matrix(frequency, communities)
            ideal = matrix.blockdiag(communities)
            row_errors = np.sqrt(np.mean((ideal-reordered)**2, axis=0))
            row_maxima = np.sqrt(np.mean((ideal-random_probability(communities, datalen))**2, axis=0))
            belongingness = 1-row_errors/row_maxima
            figb, axb = plt.subplots(3, 1, figsize=(5, 15))
            axb[0].pcolormesh(frequency)
            axb[1].pcolormesh(reordered)
            axb[2].plot(row_errors)
            axb[2].plot(row_maxima)
            axb[2].plot(belongingness)
            axb[2].set_xlabel('Index')
            axb[2].set_ylabel('Error, B_i')
            figb.savefig(f'{figdir}/{n}belonging_errors.png', dpi=300, bbox_inches=0)
            plt.close(figb)
            nbins = 40
            # Plot belongingness
            fign, axn = plt.subplots(figsize=(3, 2.2))
            axn.hist(belongingness, density=True, bins=np.linspace(0, 1, nbins+1))
            amin, amax = axn.get_ylim()
            axn.set_xlim([0, 1])
            axn.plot([robustness[int(n)-2], robustness[int(n)-2]], [amin, amax], color='black')
            axn.plot([np.mean(belongingness), np.mean(belongingness)], [amin, amax], color='red')
            print(robustness[int(n)-2], np.mean(belongingness))
            axn.set_ylim([amin, amax])
            fign.savefig(f'{figdir}/{n}cluster_belonging.png', dpi=300, bbox_inches=0)
            fign.savefig(f'{figdir}/{n}cluster_belonging.eps', transparent=True, bbox_inches=0)
            plt.close(fign)
            # Reorder by belongingness
            sorted_comms = []
            comm_belonging = []
            all_belonging = []
            cstart = 0
            for c in communities:
                Bc = [(ci, belongingness[cstart+i]) for i, ci in enumerate(c)]
                Bc = sorted(Bc, reverse=True, key=lambda x : x[1])
                sorted_comms.append([b[0] for b in Bc])
                cstart += len(c)
                comm_belonging.append(np.mean(np.array(Bc)[:, 1]))
                all_belonging.append(np.array(Bc)[:, 1].ravel())
            cbsorted = sorted(zip(sorted_comms, comm_belonging), reverse=True, key=lambda x : x[1])
            sorted_comms = [c for c, _ in cbsorted]
            sorted_belong = [b for b, _ in sorted(zip(all_belonging, comm_belonging), reverse=True, key=lambda x : x[1])]
            # Plot the output communities
            fig, ax = plt.subplots(figsize=(3, 3))
            matrix.plot_ordered(frequency, sorted_comms, ax, sort=False, cmap='bone', boxcolor=(0.9, 0.1, 0.1, 1))
            plt.axis('off')
            fig.savefig(f'{figdir}/{n}clusters.png', dpi=600, bbox_inches='tight', transparent=True)
            # fig.savefig(f'{figdir}/{n}clusters.eps', bbox_inches='tight', transparent=True)
            plt.close(fig)
            fig, ax = plt.subplots(figsize=(3, 3))
            ax.plot(np.concatenate(sorted_belong))
            fig.savefig(f'{figdir}/{n}cluster_sorted_belongingness.png', dpi=900, bbox_inches='tight', transparent=True)
            plt.close(fig)
        fig2, ax2 = plt.subplots(figsize=(2.6, 1.2))
        ax2.plot(data['n_clusters'], cost_values, linewidth=2)
        ax2.scatter(data['n_clusters'], cost_values)
        ax2.plot(data['n_clusters'], max_errors, linewidth=2)
        ax2.scatter(data['n_clusters'], max_errors)
        ax2.set_xticks(data['n_clusters'])
        ax2.set_xlabel('Number of clusters')
        ax2.set_ylabel('Mean error')
        fig2.savefig(f'{figdir}/errorvals.png', dpi=300, transparent=True)
        fig2.savefig(f'{figdir}/errorvals.eps', transparent=True)
        plt.close(fig2)
        fig3, ax3 = plt.subplots(figsize=(2.6, 1.2))
        ax3.plot(data['n_clusters'], robustness, linewidth=2)
        ax3.scatter(data['n_clusters'], robustness)
        ax3.set_xlabel('Number of clusters', fontsize=10)
        ax3.set_ylabel('Robustness', fontsize=10)
        ax3.set_xticks([2, 3, 4, 5, 6])
        ax3.set_xticklabels([2, 3, 4, 5, 6], fontsize=8)
        # ax3.set_yticks([0.3, 0.4, 0.5])
        # ax3.set_yticklabels([0.3, 0.4, 0.5], fontsize=8)
        fig3.savefig(f'{figdir}/robustness.png', dpi=300, bbox_inches=0, transparent=True)
        fig3.savefig(f'{figdir}/robustness.eps', bbox_inches=0, transparent=True)
        plt.close(fig3)
        outdict = {'finallabels':labeloutput,
            'cooccurrence':freqout}
        save_data(outdict, f'{loaddir}/finallabels.pkl')