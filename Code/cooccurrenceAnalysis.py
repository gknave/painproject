import itertools
import numpy as np
import matplotlib.pyplot as plt
from scipy import io
from scipy import interpolate
from scipy import optimize
from utils import adj_from_labels,matrix_kmeans,all_errors,modularity

def get_errors(k, clusters, dictin):
    c = clusters-2
    all_labels = dictin['out_details'][0, k][0, 0][2][c, :, :]
    adjacency = adj_from_labels(all_labels)
    communities = matrix_kmeans(adjacency, n_clusters=clusters)
    return all_errors(adjacency, communities, norm=1)

def cdf(set):
    n = len(set)
    x = np.sort(set)
    y = np.linspace(0, 1, n)
    spline = interpolate.interp1d(x, y, kind='previous', bounds_error=False, fill_value = (0, 1), assume_sorted=True) 
    return spline

def kstest_confidence(set1, set2):
    # get set lengths
    m = len(set1)
    n = len(set2)
    # generate cdfs from sets
    f1 = cdf(set1)
    f2 = cdf(set2)
    # Find the maximum difference between the two functions
    def objective(x):
        return -np.abs(f1(x)-f2(x))
    res = optimize.minimize_scalar(objective, bounds=[0, 1], method='bounded')
    # Compare this distance with the K-S test function to get the confidence, alpha
    ksD = res.x
    alpha = 2*np.exp(-ksD**2 * n*m/(n+m))
    return alpha

datapath = '../Data/'
figpath = '../Figures/'

infile = 'realdata_integral2'
outfile = infile

# stdlist = [0.1, 0.2, 0.25, 0.3, 0.33, 0.40, 0.50, 0.60, 0.67, 1.0, 2.0]
# filelist = [f'3cluster_std{val:1.2f}' for val in stdlist]

infile = outfile+'.mat'
dictin = io.loadmat(datapath+infile)

test_cases = dictin['test_cases'].ravel()

cost_values = np.zeros(test_cases.shape+(4,))
Q_values = np.zeros(test_cases.shape+(4,))
for k, case in enumerate(test_cases):
    for clusters in range(4):
        all_labels = dictin['out_details'][0, k][0, 0][1][clusters, :, :]
        adjacency = adj_from_labels(all_labels)
        communities = matrix_kmeans(adjacency, n_clusters=clusters+2)
        errors = all_errors(adjacency, communities, norm=2)
        cost_values[k, clusters] = np.sum(errors)/adjacency.shape[0]**2
        Q_values[k, clusters] = modularity(adjacency, communities)

if 'cost_values' not in dictin.keys() or 'modularity' not in dictin.keys():
    dictin['cost_values'] = cost_values
    dictin['modularity'] = Q_values
    io.savemat(datapath+infile, dictin, appendmat=False)

print(cost_values)
print(np.argmin(cost_values, axis=-1).T+2)
print(dictin.keys())

fig, ax = plt.subplots(4, 3, sharex=True, sharey=True)
ax = ax.ravel()
for k, case in enumerate(test_cases):
    ax[k].plot([2, 3, 4, 5], cost_values[k, :], '-o', color='black')
    ax[k].grid('on')
    ax[k].set_xlim([1.9, 5.1])
    ax[k].set_xticks([2, 3, 4, 5])
    ax[k].set_title(f'Noise={case:1.2f}')
fig.savefig(figpath+outfile+'_total_errors.png', transparent=True, bbox_inches='tight')
plt.close('all')

fig, ax = plt.subplots(4, 3, sharex=True, sharey=True)
ax = ax.ravel()
for k, case in enumerate(test_cases):
    ax[k].plot([2, 3, 4, 5], Q_values[k, :], '-o', color='black')
    ax[k].grid('on')
    ax[k].set_xlim([1.9, 5.1])
    ax[k].set_xticks([2, 3, 4, 5])
    ax[k].set_title(f'Noise={case:1.2f}')
fig.savefig(figpath+outfile+'_modularity.png', transparent=True, bbox_inches='tight')
plt.close('all')

# fig, ax = plt.subplots()
# k = 7 # corresponds to i = 1, j = 2
# bins = np.linspace(0, 1, 100)
# err2 = get_errors(k, 2, dictin).ravel()
# counts2, _ = np.histogram(err2, bins=bins)
# cdf2 = np.cumsum(counts2/len(err2))
# err3 = get_errors(k, 3, dictin).ravel()
# counts3, _ = np.histogram(err3, bins=bins)
# cdf3 = np.cumsum(counts3/len(err3))
# print(kstest_confidence(err2, err3))
# plt.plot(bins[1:], cdf2)
# plt.plot(bins[1:], cdf3)