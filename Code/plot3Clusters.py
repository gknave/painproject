import numpy as np
import matplotlib.pyplot as plt
from scipy import io

datapath = './Data/'
figpath = './Figures/'

stdlist = [0.1, 0.2, 0.25, 0.3, 0.33, 0.4, 0.5, 0.6, 0.67, 1.0, 2.0]
# stdlist = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 1.0, 2.0]

def plotcircle(ax, x, y, radius, N=250, **kwargs):
    th = np.linspace(0, 2*np.pi, N)
    r = np.ones(th.shape)*radius
    ax.plot(x+r*np.cos(th), y+r*np.sin(th), **kwargs)

for std in stdlist:
    filename = f'3cluster_std{std:1.2f}'
    dictin = io.loadmat(datapath+filename+'.mat')
    data = dictin['data']
    fig, ax = plt.subplots()
    ax.scatter(data[:100, 0], data[:100, 1], color='red')
    ax.scatter(data[100:-100, 0], data[100:-100, 1], color='purple')
    ax.scatter(data[-100:, 0], data[-100:, 1], color='blue')
    for (z, color) in zip(range(1, 3), [(0.2, 0.2, 0.2), (0.6, 0.6, 0.6)]):
        for x in [-1, 0, 1]:
            plotcircle(ax, x, 0, z*std, color=color)
    ax.set_aspect('equal')
    ax.set_xlabel('x_1')
    ax.set_ylabel('x_2')
    plt.savefig(f'{figpath}{filename}_scatterplot.png', bbox_inches='tight')
    plt.close('all')