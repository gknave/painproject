import numpy as np
import matplotlib.pyplot as plt
from utils import matrix,affinity
import utils.clustermetrics as clm
from sklearn import cluster
import os
 

def clusterpain(dataset, transform, metric, name, n_clusters = [2, 3, 4, 5], savefigs=False):
    # Takes in data, a transform, and a distance metric
    # Generates plots of clustered data
    # Outputs tuple of n_clusters with labels
    figpath = '../Figures'
    folder = f'{figpath}/{name}'
    if not os.path.isdir(folder):
        os.mkdir(folder)

    data = transform(dataset)
    labels = np.zeros((len(n_clusters), len(data)), dtype=int)
    dunn = np.zeros((len(n_clusters),))
    db = np.zeros_like(dunn)


    for i, n in enumerate(n_clusters):
        fig, ax = plt.subplots(figsize=(1.8, 1.8))
        
        affinity_matrix = affinity.normalized_affinity(data, dist=metric)

        clustering = cluster.SpectralClustering(n_clusters=n, affinity='precomputed')
        templabels = clustering.fit_predict(affinity_matrix)
        labels[i, :] = matrix.relabel(data, templabels)

        matrix.plot_ordered(affinity_matrix, labels[i, :], ax, cmap='viridis')
        ax.axis('off')

        if savefigs:
            fig.savefig(f'{folder}/{n}cluster.eps', dpi=300, bbox_inches='tight', transparent=True)
        
        dunn[i] = clm.dunn(data, labels[i, :], dist=metric)
        db[i] = clm.davies_bouldin(data, labels[i, :], dist=metric)

    fig2, ax2 = plt.subplots(figsize=(1.8, 1.8))
    ax2.plot(n_clusters, db, '-o', color='orange', markeredgecolor='black')
    ax2.plot(n_clusters, dunn, '-o', color='purple', markeredgecolor='black')
    ax2.scatter(np.argmin(db)+2, np.min(db), color='white', ec='black', s=200, marker='s', zorder=-2)
    ax2.scatter(np.argmax(dunn)+2, np.max(dunn), color='white', ec='black', s=200, marker='s', zorder=-2)
    ax2.set_ylabel('Dunn, DB')
    ax2.set_xlabel('Number of clusters')

    if savefigs:
        fig2.savefig(f'{folder}/DB_and_Dunn.eps', transparent=True, bbox_inches='tight')

if __name__ == '__main__':
    from utils import datasets
    from utils import transforms as tf

    clusterpain(datasets.window_data(), tf.interpolate_data, affinity.manhattan, 'real_nninterp_manhattan', savefigs=True)