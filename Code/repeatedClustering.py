import multiprocessing as mp
import numpy as np
from scipy import stats
# import matplotlib.pyplot as plt
from sklearn import cluster
import umap
import os
import timeit
import itertools
from scipy import io
import tqdm
from utils import group_windowed, relabel, centered_hist_windowed, spline_windowed, interpolated_synth

datapath = '../Data/'

## Three clusters
# threeclusterdata = np.vstack([
    # np.random.normal(loc=1.25, scale=1.0, size=(100, 10)),
    # np.random.normal(loc=0, scale=1.0, size=(100, 10)),
    # np.random.normal(loc=-1.25, scale=1.0, size=(100, 10))
    # ])
# Uniform random
# purerandom = np.random.uniform(size=(500, 10))
## Normal random
# normalrandom = np.random.normal(size=(500, 10))
## Evenly spaced data
# def evenlyspaced(length, dim):
    # ar = np.linspace(0, 1, length)
    # mesh = np.meshgrid(*[ar for k in range(dim)])
    # data = np.array([m.flatten() for m in mesh]).T
    # return data
## Windowed with bulk properties
# windowed = group_windowed()
# realdata = windowed[:, :-1]

## Windowed with centered histogram
# cent_hist = centered_hist_windowed()
# histdata = cent_hist[:, :-1]

## Windowed with nearest-interpolated spline
spline_data = spline_windowed()

def hausdorff(row_i, row_j):
    return (10-np.max(np.abs(row_i-row_j)))/10

def integral(row_i, row_j):
    mean_difference = np.sum(np.abs(row_i-row_j))/len(row_i)
    return (10-mean_difference)/10

def cosine(row_i, row_j):
    return np.sum(np.dot(row_i, row_j))/(np.sqrt(np.dot(row_i, row_i))*np.sqrt(np.dot(row_j, row_j)))

data = spline_data[:, :-1]
outname = 'realdata_hausdorff2.mat'
similarity = hausdorff


clusters=[2,3,4,5]

def affinity(data, similarity=hausdorff):
    Nsamples = len(data[:, 0])
    affmatrix = np.zeros((Nsamples, Nsamples))
    for i in range(Nsamples):
        for j in range(i, Nsamples):
            row_i = data[i, :]
            row_j = data[j, :]
            affinity_ij = similarity(row_i, row_j)
            affmatrix[i, j] = affinity_ij
            affmatrix[j, i] = affinity_ij
    affmatrix[affmatrix<=0] = 0
    affmatrix[affmatrix>=1] = 1
    return affmatrix

def consistency(x, data=data, clusters=clusters,  num_trials=100):
    noise = x
    num_samples = len(data[:, 0])
    all_labels = np.zeros((len(clusters), num_trials, num_samples))
    for trial in range(num_trials):
        trialdata = data+np.random.random(size=data.shape)*noise
        affinity_matrix = affinity(trialdata, similarity=integral)
        #Spectral clustering
        for k, n_clusters in enumerate(clusters):
            clustering = cluster.SpectralClustering(n_clusters=n_clusters, affinity='precomputed').fit(affinity_matrix)
            all_labels[k, trial, :] = relabel(trialdata, clustering.labels_)
    counts = np.zeros((len(clusters), num_samples))
    for j in range(len(clusters)):
        for k in range(num_samples):
            counts[j, k] = np.sum(all_labels[j, :, k]==stats.mode(all_labels[j, :, k]))/num_trials
    return (all_labels, [np.sum(counts[j, :])/num_samples for j in range(len(clusters))])

if __name__ == '__main__':
    with mp.Pool(processes=8) as pool:
        test_cases = [0.01, 0.025, 0.05, 0.075, 0.1, 0.25, 0.5, 1, 2, 3, 5, 8]
        output = np.zeros((len(clusters), len(test_cases)))
        out_details = []
        for i, res in tqdm.tqdm(enumerate(pool.imap(consistency, test_cases)), total=len(test_cases)):
            out_details.append({'i':i, 'labels':res[0]})
            output[:, i] = res[1]
        out = output.reshape((len(clusters), len(test_cases)))
        io.savemat(datapath+outname, {'out_details':out_details, 'data':data, 'output':out, 'test_cases':test_cases})