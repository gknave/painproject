from scipy import io
import umap
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

infile = 'threeCluster2.mat'
outtag = 'threeCluster2'
clim = [0.4, 1.0]

def rescale(outvals, tmin):
    return (np.asarray(outvals)-tmin)/(1-tmin)

dictin = io.loadmat(infile)
output = dictin['output']
data = dictin['data']
out_details = dictin['out_details']

dists = [0.0, 0.01, 0.05, 0.1, 0.5, 1]
neighbors = [5, 15, 30, 50, 100]

fig = plt.figure(figsize=(8, 3.5))
#bfig, bax = plt.subplots(1, 1)
#fig, ax = plt.subplots(2, 2)
splist = [1, 2, 4, 5]
for j in range(4):
    tmin = 1/(2+j) #theoretical minimum is 1/num_clusters
    ax = fig.add_subplot(2, 3, splist[j])
    ax.pcolormesh(rescale(output[j, :, :].T,tmin), vmin=clim[0], vmax=clim[1])
    ax.invert_yaxis()
    ax.set_xticks([k+0.5 for k in range(len(dists))])
    ax.set_yticks([k+0.5 for k in range(len(neighbors))])
    if j in [0, 2]:
        ax.set_yticklabels(neighbors)
        ax.set_ylabel('n_neighbors')
    else: 
        ax.set_yticklabels([])
    if j in [2, 3]:
        ax.set_xticklabels(dists)
        ax.set_xlabel('min_dist')
    else: 
        ax.set_xticklabels([])
cax = fig.add_subplot(1, 3, 3)
cax.axis('off')
c = fig.colorbar(cm.ScalarMappable(cmap='viridis'), values=np.linspace(clim[0], clim[1], 20), ax=cax)
plt.savefig(f'../Figures/TrialComparison_{outtag}.eps', bbox_inches=0, transparent=True)

plt.figure()
umapprocess = umap.UMAP(n_neighbors=5)
u = umapprocess.fit_transform(data)
u = u/np.std(u)
plt.scatter(u[:, 0], u[:, 1])

plt.figure(figsize=(3.5, 2.5))
means = []
medians = []
for j in range(4):
    means.append(rescale(np.mean(output[j, :, :].ravel()), 1/(2+j)))
    medians.append(rescale(np.median(output[j, :, :].ravel()), 1/(2+j)))
plt.plot([2, 3, 4, 5], means, '-or')
plt.plot([2, 3, 4, 5], medians, '-ok')
plt.xlim([1.9, 5.1])
plt.ylim(clim)
plt.xlabel('Number of clusters')
plt.ylabel('Median consistency')
plt.savefig(f'../Figures/MedianConsistency_{outtag}.eps', bbox_inches='tight', transparent=True)
plt.close('all')
#plt.show()
