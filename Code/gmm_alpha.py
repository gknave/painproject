import numpy as np
from scipy import optimize
import utils.gaussians as gs
import matplotlib.pyplot as plt
plt.rcParams.update({
    "text.usetex": True})
    
if __name__=='__main__':
    separations = np.linspace(0.1, 6, 71)
    n_trials = 10
    alpha_list = np.zeros((len(separations), n_trials))
    for k, sep in enumerate(separations):
        guess = [0.5, 0.4, 0.5, 2.5, 0.5]
        for j in range(n_trials):
            mydata = np.concatenate((np.random.normal(loc=0, size=(100,)), 
                                     np.random.normal(loc=sep, size=(100,))))
            res = optimize.minimize(gs.gaussmixture_ll, guess, args=(mydata,), method='Nelder-Mead')
            alpha_list[k, j] = res.x[0]
    alphas = np.where(alpha_list>0.5, alpha_list, 1-alpha_list)
    fig, ax = plt.subplots(figsize=(3,2))
    mid = np.median(alphas, axis=-1)
    print(mid.shape)
    upper = np.max(alphas, axis=-1)
    lower = np.min(alphas, axis=-1)
    yerr = np.vstack([mid-lower, upper-mid])
    ax.fill_between(separations, lower, upper, color='orange')
    ax.plot(separations, mid, '-o', color='black', linewidth=3)
    ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
    ax.set_ylabel(r'$\alpha$')
    ax.set_xticks(list(range(7)))
    ax.set_xlabel(r'$\mu_2-\mu_1$')
    fig.savefig('../Figures/gmm_alpha.eps', bbox_inches='tight', transparent=True)
    fig.savefig('../Figures/gmm_alpha.png', dpi=300, bbox_inches='tight', transparent=False)
    plt.show()