import numpy as np
from utils import matrix
import matplotlib.pyplot as plt
import seaborn as sns

figpath = './Figures'
coms = [list(range(0, 100)), list(range(100, 300))]

if __name__ == '__main__':
    adjacency = np.zeros((300, 300))
    for c in coms:
        for ci in c:
            adjacency[ci, c] = 1
    fig, ax = plt.subplots(figsize=(3, 3))
    # matrix.plot_ordered(adjacency, coms, ax, sort=False, cmap='bone', boxcolor=sns.color_palette()[3])
    matrix.plot_ordered(adjacency, coms, ax, sort=False, cmap='bone', boxcolor=(0.9, 0.1, 0.1, 1))
    plt.axis('off')
    fig.savefig(f'{figpath}/idealmatrix.png', dpi=600, transparent=True, bbox_inches='tight')
    plt.close(fig)  