#!/usr/bin/perl

@ds = ("real","synth");
@tf = ("bulk","hist","interp_n","interp_l");

for $d (@ds) {
    for $t (@tf) {
        print `echo "$d $t"`;
        print `python repeatedUMAPclustering.py $d $t`;
    }
}

