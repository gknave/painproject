import numpy as np
from scipy.special import gamma

def euclidean(row_i, row_j):
    difference = row_i-row_j
    norm = np.sqrt(np.dot(difference, difference))
    return norm
      
def where(condition):
    inds = [i for (i, c) in enumerate(condition) if c]
    return inds

### Functions to check clustering validity
def rss(data, cc, labels):
    # residual sum of squares. Useful for other metrics
    error = 0
    for (xy, l) in zip(data, labels):
        d = xy-cc[l, :]
        error += np.dot(d, d)
    return error

def rss_rescale(data, cc, labels):    
    # residual sum of squares. Useful for other metrics
    center = np.mean(data, axis=0)
    totaldiff = 0
    for xy in data:
        d = xy-center
        totaldiff += np.dot(d, d)
    var = totaldiff/(labels.shape[0]-1)
    error = 0
    for (xy, l) in zip(data, labels):
        d = xy-cc[l, :]
        error += np.dot(d, d)/var
    return error
    
def normal_ll(data, labels):
    l_list = np.unique(labels)
    output = 0
    for l in l_list:
        subset = data[labels==l, :]
        n = subset.shape[0]
        c = np.mean(subset, axis=0)
        rss = 0
        for k in range(n):
            xy = subset[k, :]
            d = xy-c
            rss += np.dot(d, d)
        sigma = np.sqrt(rss/(n-1))
        output -= n*np.log(sigma*np.sqrt(2*np.pi)) + (n-1)/2
    return output
        
def beta_ll(data, labels):
    l_list = np.unique(labels)
    output = 0
    for l in l_list:
        subset = data[labels==l, :]
        n = subset.shape[0]
        c = np.mean(subset, axis=0)
        rss = 0
        for k in range(n):
            xy = subset[k, :]
            d = xy-c
            rss += np.dot(d, d)
        sigma = np.sqrt(rss/(n-1))
        a = mu*(mu*(1-mu)/sigma**2-1)
        b = (1-mu)*(mu*(1-mu)/sigma**2-1)
        for k in range(n):
            xy = subset[k, :]
            output += np.log(beta())
    return output

def beta(x, a, b):
    B = gamma(a)*gamma(b)/gamma(a+b)
    if np.abs(B) > 0:
        return x**(a-1)*(1-x)**(b-1)/B
    else:
        return x**(a-1)*(1-x)**(b-1)/1e-12

def aic(data, labels):
    _, M = data.shape
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = where(labels==l)
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    return -2*normal_ll(data, labels) + 2*K*M

def bic(data, labels):
    N, M = data.shape
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = where(labels==l)
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    return -2*normal_ll(data, labels) + np.log(N)*K*M

def dunn(data, labels, dist=euclidean):
    N, M = data.shape
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = where(labels==l)
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    S = np.zeros((K,))
    T = np.zeros((K,))
    for (xy, l) in zip(data, labels):
        T[l] += 1
        S[l] += dist(xy, cc[l, :])
    for l in range(K):
        S[l] /= T[l]
    Alist = []
    for i in range(K):
        for j in range(i+1, K):
            Alist.append(dist(cc[i, :], cc[j, :]))
    DI = np.min(Alist)/np.max(S)
    return DI


def davies_bouldin(data, labels, dist=euclidean):
    N, M = data.shape
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = where(labels==l)
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    S = np.zeros((K,))
    T = np.zeros((K,))
    for (xy, l) in zip(data, labels):
        T[l] += 1
        S[l] += dist(xy, cc[l, :])
    for l in range(K):
        S[l] /= T[l]
    R = np.zeros((K, K))
    for i in range(K):
        for j in range(i+1, K):
            Aij = dist(cc[i, :], cc[j, :])
            R[i, j] = (S[i]+S[j])/Aij
            R[j, i] = (S[i]+S[j])/Aij
    DB = 0
    for j in range(K):
        DB += np.max(R[j, :])
    DB = DB/K
    return DB
    