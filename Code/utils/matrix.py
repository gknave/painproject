import numpy as np
from sklearn import cluster
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from matplotlib.patches import FancyBboxPatch

def where(condition):
    inds = [i for (i, c) in enumerate(condition) if c]
    return inds

##### Functions used for assessing the co-occurrence matrix
def adj_from_labels(all_labels):
    num_samples = all_labels.shape[1]
    num_trials = all_labels.shape[0]
    adjacency = np.ones((num_samples, num_samples))
    for i in range(num_samples):
        for j in range(i+1, num_samples):
            weight = np.sum(all_labels[:, i]==all_labels[:, j])/num_trials
            adjacency[i, j] = weight
            adjacency[j, i] = weight
    return adjacency

def matrix_kmeans(adjacency, n_clusters=2):
    clustering = cluster.SpectralClustering(n_clusters=n_clusters, affinity='precomputed').fit(adjacency)
    labels = clustering.labels_
    communities = [list(np.argwhere(labels==j).ravel()) for j in np.unique(labels)]
    return communities

def reorder_matrix(adjacency, communities):
    size = adjacency.shape[0]
    reindex = []
    for com in communities:
        reindex += com
    reordered = np.ones((size, size))
    for i in range(size):
        for j in range(i+1, size):
            m = reindex[i]
            n = reindex[j]
            reordered[i, j] = adjacency[m, n]
            reordered[j, i] = adjacency[m, n]
    return reordered

def blockdiag(communities):
    comlens = [len(com) for com in communities]
    out = np.zeros((np.sum(comlens), np.sum(comlens)))
    start = 0
    for length in comlens:
        out[start:start+length, start:start+length] = 1
        start += length
    return out

def mean_error(adjacency, communities, norm=1):
    reordered = reorder_matrix(adjacency, communities)
    ideal = blockdiag(communities)
    difference = ideal-reordered
    return (np.mean(np.abs(difference.ravel())**norm))**(1/norm)

def modularity(adjacency, communities):
    k = np.sum(adjacency, axis=-1)
    m = np.sum(k)/2
    Q = 0
    for com in communities:
        for c in com:
            for d in com:
                B = adjacency[c, d]-k[c]*k[d]/(2*m)
                Q += B/(2*m)
    return Q

def relabel(data, labels):
    # Takes in transformed data
    # Relabels based on mean of first dimension of data
    labels = labels.astype(int)
    N = len(np.unique(labels))
    midlist = np.zeros((N,))
    for c in range(N):
        clist = where(labels==c)
        midlist[c] = np.mean(data[clist, 0].ravel())
    pairs = list(enumerate(midlist))
    sortlabels = sorted(pairs, key=lambda x: x[1])
    relabels = np.zeros(labels.shape, dtype=int)
    for new_l, (l, _) in enumerate(sortlabels):
        relabels = np.where(labels==l, new_l, relabels)
    return relabels

def communities(labels):
    coms = []
    for c in np.unique(labels):
        coms.append(list(where(labels==c)))
    return coms

def labels(communities):
    num_elements = np.sum([len(c) for c in communities])
    labels = np.zeros((num_elements,))
    for i, c in enumerate(communities):
        labels[c] = i
    return labels

def add_fancy_patch_around(ax, bb, boxcolor=(0, 0, 0, 1), **kwargs):
    fancy = FancyBboxPatch((bb.xmin, bb.ymin), bb.width, bb.height,
                           fc=(1, 1, 1, 0), ec=boxcolor,
                           linewidth=2,
                           **kwargs)
    ax.add_patch(fancy)
    return fancy

def community_boxes(communities, axis, boxcolor=(0, 0, 0, 1)):
    tot_len = 0
    for c in communities:
        bb = mtransforms.Bbox([[tot_len, tot_len], [tot_len+len(c), tot_len+len(c)]])
        fancy = add_fancy_patch_around(axis, bb, boxstyle="round,pad=0.1", boxcolor=boxcolor)
        tot_len += len(c)

def plot_ordered(affinity_matrix, coms, axis, sort=True, cmap='viridis', boxcolor=(0, 0, 0, 1)):
    if sort:
        coms.sort(key=len, reverse=True)
    reordered = reorder_matrix(affinity_matrix, coms)
    axis.pcolormesh(reordered, cmap=cmap, vmin=0, vmax=1)
    community_boxes(coms, axis, boxcolor=boxcolor)
    axis.invert_yaxis()
    axis.set_aspect('equal')
