import numpy as np
from scipy import optimize
from scipy.special import gamma

# Easy enough to just write my own Gaussian
def gaussian(x, c=0, sigma=1):
    return np.exp(-((x-c)/sigma)**2/2) / (np.abs(sigma)*np.sqrt(2*np.pi))

def beta(x, a=0.5, b=0.5):
    B = gamma(a)*gamma(b)/gamma(a+b)
    return x**(a-1)*(1-x)**(b-1)/B

# Log likelihood of a Gaussian mixture model, given parameters
## data is a keyword parameter to fit scipy optimization functions 
## defaults to none 
def gaussmixture_ll(params, data=None):
    if data is None:
        raise ValueError("Please specify data!")
    # For n gaussians, there are n-1 weight parameters, n means, and n standard deviations
    # This gives 3n-1 parameters
    n = int((params.shape[0]+1)/3)
    # Figure out last weight given other weights and concatenate
    weights = params[:n-1]
    lastw = 1-np.sum(weights)
    allweights = np.concatenate((weights, [lastw,]))
    # Extract mean, standard deviation parameters
    p_array = params[n-1:].reshape((n,2))
    ll = 0
    for x in data:
        f = 0
        for (w, p) in zip(allweights, p_array):
            f += w*gaussian(x, c=p[0], sigma=np.abs(p[1]))
        ll += np.log(f)
    # Impose bounds on weighting parameters
    if np.any(allweights>1):
        return 1e12+1000*np.max(allweights)
    elif np.any(allweights<0):
        return 1e12-1000*np.min(allweights)
    else:
        return -ll

def betamixture_ll(params, data=None):
    if data is None:
        raise ValueError("Please specify data!")
    # For n gaussians, there are n-1 weight parameters, n means, and n standard deviations
    # This gives 3n-1 parameters
    n = int((params.shape[0]+1)/3)
    # Figure out last weight given other weights and concatenate
    weights = params[:n-1]
    lastw = 1-np.sum(weights)
    allweights = np.concatenate((weights, [lastw,]))
    # Extract mean, standard deviation parameters
    p_array = params[n-1:].reshape((n,2))
    ll = 0
    for x in data:
        f = 0
        for (w, p) in zip(allweights, p_array):
            temp_f = w*beta(x, a=p[0], b=p[1])
            f += temp_f
        if f <= 0:
            ll += 1e8
        else:
            ll += np.log(f)
    # Impose bounds on weighting parameters
    if np.any(allweights>1):
        return 1e12+1000*np.max(allweights)
    elif np.any(allweights<0):
        return 1e12-1000*np.min(allweights)
    else:
        return -ll    
    

def maxnormal_ll(params, data=None):
    if data is None:
        raise ValueError("Please specify data!")
    # For n gaussians, there are 2n parameters
    n = int(params.shape[0]/2)
    p_array = params.reshape((n,2))
    ll = 0
    for x in data:
        f = np.zeros((n,))
        for i, p in enumerate(p_array):
            f[i] = gaussian(x, c=p[0], sigma=np.abs(p[1]))/n
        ll += np.log(np.max(f))
    return -ll

def gmm_means(params, data=None, stdev=1):
    N = len(params)
    all_params = np.zeros((3*N-1))
    all_params[:N-1] = 1/N
    for n in range(N):
        i = 2*n + N-1
        all_params[i] = params[n]
        all_params[i+1] = stdev
    return gaussmixture_ll(all_params, data=data)

def gmm_even(params, data=None):
    N = int(len(params)/2)
    all_params = np.zeros((3*N-1))
    all_params[:N-1] = 1/N
    all_params[N-1:] = params
    return gaussmixture_ll(all_params, data=data)

def gmm_func(x, params=[0, 1]):
    params = np.array(params)
    N = int((len(params)+1)/3)
    wlist = np.array(list(params[:N-1])+[1-np.sum(params[:N-1]),])
    plist = params[N-1:].reshape((N, 2))
    y = np.zeros(x.shape)
    for w, p in zip(wlist, plist):
        y += w*gaussian(x, c=p[0], sigma=p[1])
    return y

def bmm_even(params, data=None):
    N = int(len(params)/2)
    all_params = np.zeros((3*N-1))
    all_params[:N-1] = 1/N
    all_params[N-1:] = params
    return betamixture_ll(all_params, data=data)

def bmm_func(x, params=[0, 1]):
    params = np.array(params)
    N = int((len(params)+1)/3)
    wlist = np.array(list(params[:N-1])+[1-np.sum(params[:N-1]),])
    plist = params[N-1:].reshape((N, 2))
    y = np.zeros(x.shape)
    for w, p in zip(wlist, plist):
        y += w*beta(x, a=p[0], b=p[1])
    return y

def norm_to_beta(mean, stdev):
    nu = (mean*(1-mean)/stdev**2-1)
    a = mean*nu
    b = (1-mean)*nu
    return a, b

def beta_to_norm(a, b):
    mean = a/(a+b)
    stdev = np.sqrt(a*b/((a+b)**2*(a+b+1)))
    return mean, stdev