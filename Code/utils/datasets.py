import numpy as np
import pandas as pd
from scipy import io

wlen = 14
wcutoff = 3

def load_data():
    datapath = '../Data'
    tmpx = io.loadmat(f'{datapath}/Clifton/PatientData_final_withheaders.mat')
    headers=[x[0] for x in tmpx['column_headers'][0]]
    patient_list=[x for x in tmpx.keys() if 'Patient' in x]
    for patient in patient_list:
        tmp=tmpx[patient]
        df=pd.DataFrame(tmp,columns=headers)
        df['Patient_ID']=patient
        collist=[df.columns[-1]]+list(df.columns[:-1])
        df=df[collist]
        if patient==patient_list[0]:
            patient_df=df.copy()
        else:
            patient_df=patient_df.append(df)
    return patient_df, patient_list

def make_dicts(meds=False):
    patient_df, patient_list = load_data()
    tdict = {}
    paindict = {}
    LAmeddict = {}
    SAmeddict = {}
    for patient in patient_list:
        inds = np.where(np.logical_and(patient_df['Patient_ID']==patient, ~patient_df['VasPain'].isnull()))[0]
        time = patient_df['days'].iloc[inds]
        pain = patient_df['VasPain'].iloc[inds]
        tdict[patient] = time.values
        paindict[patient] = pain.values
        LAinds = np.where(np.logical_and(patient_df['Patient_ID']==patient, patient_df['AnyLong']>0))[0]
        LAmeddict[patient] = patient_df['days'].iloc[LAinds].values
        SAinds = np.where(np.logical_and(patient_df['Patient_ID']==patient, patient_df['AnyShort']>0))[0]
        SAmeddict[patient] = patient_df['days'].iloc[SAinds].values
    if meds:
        return tdict, paindict, LAmeddict, SAmeddict
    return tdict, paindict

def window_data():
    tdict, paindict = make_dicts()
    window_df = pd.DataFrame(columns=['Group', 'Patient', 'Time', 'Pain', 't0'])
    group = 0
    for key in tdict.keys():
        t = tdict[key]
        pain = paindict[key]
        for tmax in np.arange(wlen, np.ceil(t[-1]/wlen)*wlen, wlen):
            tmin = tmax-wlen
            inds = np.where(np.logical_and(t>tmin, t<tmax))[0]
            datadict = {
                'Group':[group for _ in inds],
                'Patient':[key for _ in inds],
                'Time':t[inds]-tmin,
                'Pain':pain[inds],
                't0':tmin
                }
            if len(inds) > wcutoff and t[inds[-1]]-t[inds[0]]>1:
                temp_df = pd.DataFrame(data=datadict, columns=['Group', 'Patient', 'Time', 'Pain', 't0'])
                window_df = pd.concat([window_df, temp_df], axis='index')
                group += 1
    return window_df

def patient_labels():
    window_df = window_data()
    groups = int(np.max(window_df['Group'].values)+1)
    pt_labels = []
    for a, g in window_df.groupby('Group'):
         pt_labels.append(g['Patient'][0])
    return pt_labels

def get_category(cat):
    patient_df, _ = load_data()
    window_df = window_data()
    patients = patient_labels()
    groups = int(np.max(window_df['Group'].values)+1)
    cat_values = np.zeros((groups,))
    for i, patient in enumerate(patients):
        inds = np.where(patient_df['Patient_ID']==patient)[0]
        cat_values[i] = np.max(patient_df[cat].iloc[inds])
    return cat_values

def synthetic_paindata(cluster_means=[2, 5, 8], noise=1, clustersize=100):
    pain_df = pd.DataFrame(columns=['Group', 'Patient', 'Time', 'Pain'])
    group = 0
    for i, mean in enumerate(cluster_means):
        for j in range(clustersize):
            key = f'c{i}_{j}'
            samples = int(np.round(np.random.uniform(low=wcutoff, high=28)))
            time_data = np.sort(np.random.uniform(size=(samples,))*wlen)
            pain_data = np.random.normal(loc=mean, scale=noise, size=(samples,))
            pain_data[pain_data < 0] = 0
            pain_data[pain_data > 10] = 10
            datadict = {
                'Group':[group for _ in range(samples)],
                'Patient':[key for _ in range(samples)],
                'Time':time_data,
                'Pain':pain_data,
                }
            temp_df = pd.DataFrame(data=datadict, columns=['Group', 'Patient', 'Time', 'Pain'])
            pain_df = pd.concat([pain_df, temp_df], axis='index')
            group += 1
    return pain_df

def scrambled(replacement=False):
    window_df = window_data()
    pain = window_df['Pain'].values
    n = len(pain)
    if replacement:
        shuffled = np.random.randint(0, n, n)
        newpain = pain[shuffled]
    else:
        newpain = pain.copy()
        np.random.shuffle(newpain)
    out_df = window_df.copy()
    out_df['Pain'] = newpain
    return out_df

def resampled_data(data=window_data()):
    window_df = data
    labels = data['Group'].values
    n = len(np.unique(labels))
    indices = np.random.randint(0, n, n)
    out_df = pd.DataFrame(columns=['Group', 'Original_group', 'Patient', 'Time', 'Pain'])
    for k, i in enumerate(indices):
        temp_df = window_df[window_df['Group']==i].copy()
        temp_df.loc[:, 'Group'] = k
        temp_df.loc[:, 'Original_group'] = i
        out_df = pd.concat([out_df, temp_df], axis='index')
    return out_df

def uniform_random(n_samples=200):
    pain_df = pd.DataFrame(columns=['Group', 'Patient', 'Time', 'Pain'])
    for i, mean in enumerate(range(n_samples)):
        key = f'sample_{i}'
        inds = int(np.round(np.random.uniform(low=wcutoff, high=21)))
        time_data = np.sort(np.random.uniform(size=(inds,))*wlen)
        pain_data = np.random.uniform(size=(inds,))*10
        datadict = {
            'Group':[i for _ in range(inds)],
            'Patient':[key for _ in range(inds)],
            'Time':time_data,
            'Pain':pain_data,
            }
        temp_df = pd.DataFrame(data=datadict, columns=['Group', 'Patient', 'Time', 'Pain'])
        pain_df = pd.concat([pain_df, temp_df], axis='index')
    return pain_df
