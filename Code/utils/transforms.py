import numpy as np
from scipy import stats,interpolate

def bulk_props(window_df):
    groups = int(np.max(window_df['Group'].values)+1)
    windowed = np.zeros((groups, 7))
    for a, g in window_df.groupby('Group'):
        time = g['Time']
        pain = g['Pain']
        windowed[a, 0] = np.mean(pain)
        windowed[a, 1] = np.std(pain)
        windowed[a, 2] = stats.skew(pain)
        windowed[a, 3] = stats.kurtosis(pain)
        windowed[a, 4] = len(np.where(pain<=2)[0])/len(pain)
        windowed[a, 5] = len(np.where(pain>=8)[0])/len(pain)
        res = stats.linregress(time, pain)
        windowed[a, 6] = res.slope
    return windowed

def histogram(window_df):
    groups = int(np.max(window_df['Group'].values)+1)
    bins = np.arange(0, 10.1, 0.5)
    hist = np.zeros((groups, len(bins)-1))
    for a, g in window_df.groupby('Group'):
        pain = g['Pain']
        hist[a, :], _ = np.histogram(pain, bins=bins, density=True)
    return hist

def centered_hist(window_df, n_bins=19):
    groups = int(np.max(window_df['Group'].values)+1)
    cent_hist = np.zeros((groups, n_bins+2))
    for a, g in window_df.groupby('Group'):
        pain = g['Pain']
        pmin = np.min(pain)
        pmax = np.max(pain)
        pain -= np.mean(pain)
        pain /= np.std(pain)
        cent_hist[a, :-2], _ = np.histogram(pain, bins=n_bins, range=(-4, 4), density=False)
        cent_hist[a, -2] = pmin
        cent_hist[a, -1] = pmax
    return cent_hist

def interpolate_data(window_df, kind='nearest', t_steps=672):
    groups = int(np.max(window_df['Group'].values)+1)
    spline_data = np.zeros((groups, t_steps))
    t_values = np.linspace(0, 14, t_steps)
    for a, g in window_df.groupby('Group'):
        time = g['Time'].values
        pain = g['Pain'].values
        fill_value = (pain[0], pain[-1])
        spl = interpolate.interp1d(time, pain, bounds_error=False, kind=kind, fill_value=fill_value, assume_sorted=True)
        spline_data[a, :] = spl(t_values)
    return spline_data

def linear_interp(window_df, t_steps=672):
    return interpolate_data(window_df, kind='linear', t_steps=t_steps)