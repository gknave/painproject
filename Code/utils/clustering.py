import utils.transforms as tf
from utils import affinity
import numpy as np
from sklearn import cluster

def get_affmatrix(dataset, transform):
    data = transform(dataset)
    if transform==tf.bulk_props:
        standardized = affinity.standardize(data)
        affinity_matrix = affinity.normalized_affinity(standardized)
    elif transform==tf.histogram:
        affinity_matrix = affinity.normalized_affinity(np.cumsum(data, axis=-1), dist=affinity.manhattan)
    else:
        affinity_matrix = affinity.normalized_affinity(data)
    return affinity_matrix

def cluster_data(dataset, transform, clusterlist=[2, 3, 4, 5, 6]):
    random_groups = dataset['Group'].values
    groups = dataset['Original_group'].values
    group_key = []
    for pair in zip(random_groups, groups):
        if not pair in group_key:
            group_key.append(pair)
    if not hasattr(clusterlist, '__iter__'):
        clusterlist = [clusterlist,]
    affinity_matrix = get_affmatrix(dataset, transform)
    n_points = len(np.unique(dataset['Group'].values))
    label_out = np.zeros((n_points, len(clusterlist)))
    for k, n in enumerate(clusterlist):
        clustering = cluster.SpectralClustering(n_clusters=n, affinity='precomputed')
        label_out[:, k] = clustering.fit_predict(affinity_matrix)
    result = {
        'group_key':group_key,
        'affinity_matrix':affinity_matrix,
        'n_clusters':clusterlist,
        'labels':label_out
    }
    return result