import os
from utils import clustering
from utils import datasets as ds
from utils import transforms as tf
import pickle

datapath = '../Data/resamples'

num_trials=100
transform = tf.histogram
slug = 'histogram_synth'
folder = f'{datapath}/{slug}'

if __name__ == '__main__':
    synth_data = ds.synthetic_paindata() # Generate the synthetic dataset once
    if not os.path.isdir(folder):
        os.mkdir(folder)
    for trial in range(num_trials):
        savename = f'{folder}/test{trial}.pkl'
        data = ds.resampled_data(data=synth_data) # Resample the dataset regularly
        res = clustering.cluster_data(data, transform)
        with open(savename, 'wb') as fp:
            pickle.dump(res, fp, protocol=pickle.HIGHEST_PROTOCOL)
