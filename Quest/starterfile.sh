#!/bin/bash
#SBATCH -A b1020

#SBATCH -p short
#SBATCH -t 01:00:00
#SBATCH -n 30
#SBATCH --mem=6G

#SBATCH --mail-user=Gary.Nave@northwestern.edu
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE

#SBATCH --output=testout.txt
#SBATCH --error=testerr.txt

#SBATCH --job-name="test"

module purge

export PATH=$PATH:/projects/b1020/gary/pain/Code/

module load python/anaconda
