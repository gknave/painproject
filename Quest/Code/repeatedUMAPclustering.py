import argparse
import multiprocessing as mp
import numpy as np
from scipy import stats
from sklearn import cluster
import umap
import os
import timeit
import itertools
from scipy import io
from utils import datasets,matrix,transforms,affinity,clustermetrics

parser = argparse.ArgumentParser(description = "Runs repeated UMAP on a specified dataset with a specified transform")
parser.add_argument('dataset', metavar='dataset', 
        type=str, nargs=1, default='real',
        help='dataset to apply UMAP clustering to, options: "synth" or "real"')
parser.add_argument('transform', metavar='transform', 
        type=str, nargs=1, default ='bulk',
        help='Way to transform data to regular coordinates, options: \n "bulk"\n"hist"\n"interp_n"\n"interp_l"')

args = parser.parse_args()
ds_arg = args.dataset[0]
tf_arg = args.transform[0]

if ds_arg == 'real':
    dataset = datasets.window_data()
elif ds_arg == 'synth':
    dataset = datasets.synthetic_paindata()
else:
    raise Exception('dataset must be "real" or "synth".')

if tf_arg == 'bulk':
    tf = transforms.bulk_props
    kind = None
elif tf_arg == 'hist':
    tf = transforms.centered_hist
    kind = None
elif tf_arg == 'interp_n':
    tf = transforms.interpolate_data
    kind = 'nearest'
elif tf_arg == 'interp_l':
    tf = transforms.interpolate_data
    kind = 'linear'
else:
    raise Exception('transform must be "bulk","hist","interp_n", or "interp_l"')

if not kind == None:
    data = tf(dataset, kind=kind)
else:
    data = tf(dataset)

outname = f'{ds_arg}data_{tf_arg}'

clusters=[2,3,4,5]

def UMAPconsistency(x, data=data, clusters=clusters,  num_trials=100):
    min_dist = x[0]
    n_neighbors = x[1]
    num_samples = len(data[:, 0])
    all_labels = np.zeros((len(clusters), num_trials, num_samples))
    all_umaps = np.zeros((2, num_trials, num_samples))
    for trial in range(num_trials):
        trialdata = data+np.random.random(size=data.shape)*0.1
        #UMAP
        umapprocess = umap.UMAP(min_dist=min_dist, n_neighbors=n_neighbors)
        u = umapprocess.fit_transform(trialdata)
        u = u/np.std(u)
        all_umaps[:, trial, :] = u.T
        #Spectral clustering
        for k, n_clusters in enumerate(clusters):
            clustering = cluster.SpectralClustering(n_clusters=n_clusters).fit(u)
            all_labels[k, trial, :] = matrix.relabel(data, clustering.labels_)
    counts = np.zeros((len(clusters), num_samples))
    for j in range(len(clusters)):
        for k in range(num_samples):
            counts[j, k] = np.sum(all_labels[j, :, k]==stats.mode(all_labels[j, :, k]))/num_trials
    return (all_umaps, all_labels, [np.sum(counts[j, :])/num_samples for j in range(len(clusters))])

if __name__ == '__main__':
    with mp.Pool(processes=8) as pool:
        dists = [0.0, 0.01, 0.05, 0.1, 0.5, 1]
        neighbors = [5, 15, 30, 50, 100]
        test_cases = itertools.product(dists, neighbors)
        output = np.zeros((len(clusters), len(dists)*len(neighbors),))
        out_details = []
        for i, res in enumerate(pool.imap(UMAPconsistency, test_cases)):
            out_details.append({'i':i, 'umap':res[0], 'labels':res[1]})
            output[:, i] = res[2]
        out = output.reshape((len(clusters), len(dists), len(neighbors)))
        io.savemat(datapath+outname, {'out_details':out_details, 'data':data, 'output':out})
