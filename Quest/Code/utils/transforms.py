import numpy as np
from scipy import stats,interpolate

def bulk_props(window_df):
    groups = int(np.max(window_df['Group'].values)+1)
    windowed = np.zeros((groups, 7))
    for a, g in window_df.groupby('Group'):
        pain = g['Pain']
        windowed[a, 0] = np.mean(pain)/10
        windowed[a, 1] = np.std(pain)/5
        windowed[a, 2] = stats.skew(pain)/5
        windowed[a, 3] = stats.kurtosis(pain)/10
        windowed[a, 4] = len(np.where(pain<=3)[0])/len(pain)
        windowed[a, 5] = len(np.where(pain>=8)[0])/len(pain)
        windowed[a, 6] = np.mean(np.gradient(pain))/5
    return windowed

def centered_hist(window_df):
    groups = int(np.max(window_df['Group'].values)+1)
    cent_hist = np.zeros((groups, 22))
    for a, g in window_df.groupby('Group'):
        pain = g['Pain']
        pmin = np.min(pain)
        pmax = np.max(pain)
        pain -= np.mean(pain)
        pain /= np.std(pain)
        cent_hist[a, :-2], _ = np.histogram(pain, bins=20, range=(-4, 4), density=False)
        cent_hist[a, -2] = pmin
        cent_hist[a, -1] = pmax
    return cent_hist

def interpolate_data(window_df, kind='nearest', t_steps=672):
    groups = int(np.max(window_df['Group'].values)+1)
    spline_data = np.zeros((groups, t_steps))
    t_values = np.linspace(0, 14, t_steps)
    for a, g in window_df.groupby('Group'):
        time = g['Time'].values
        pain = g['Pain'].values
        fill_value = (pain[0], pain[-1])
        spl = interpolate.interp1d(time, pain, bounds_error=False, kind=kind, fill_value=fill_value, assume_sorted=True)
        spline_data[a, :] = spl(t_values)
    return spline_data
