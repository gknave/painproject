import numpy as np

### Functions to check clustering validity
def rss(data, cc, labels):
    error = 0
    for (xy, l) in zip(data, labels):
        d = xy-cc[l, :]
        error += np.dot(d, d)
    return error

def aic(data, labels):
    _, M = data.shape()
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = np.where(labels==l)[0]
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    return rss(data, cc, labels) + 2*K*M

def bic(data, labels):
    N, M = data.shape()
    l_list = np.unique(labels)
    K = len(l_list)
    cc = np.zeros((K, M))
    for l in l_list:
        indlist = np.where(labels==l)[0]
        cc[l, :] = np.mean(data[indlist, :], axis=0)
    return rss(data, cc, labels) + np.log(N)*K*M
