import numpy as np
import pandas as pd
from scipy import io

wlen = 14
wcutoff = 3

def load_data():
    datapath = '/projects/b1020/gary/pain/Code'
    tmpx = io.loadmat(f'{datapath}/PatientData_final_withheaders.mat')
    headers=[x[0] for x in tmpx['column_headers'][0]]
    patient_list=[x for x in tmpx.keys() if 'Patient' in x]
    for patient in patient_list:
        tmp=tmpx[patient]
        df=pd.DataFrame(tmp,columns=headers)
        df['Patient_ID']=patient
        collist=[df.columns[-1]]+list(df.columns[:-1])
        df=df[collist]
        if patient==patient_list[0]:
            patient_df=df.copy()
        else:
            patient_df=patient_df.append(df)
    return patient_df, patient_list

def make_dicts():
    patient_df, patient_list = load_data()
    tdict = {}
    paindict = {}
    for patient in patient_list:
        inds = np.where(np.logical_and(patient_df['Patient_ID']==patient, ~patient_df['VasPain'].isnull()))[0]
        time = patient_df['days'].iloc[inds]
        pain = patient_df['VasPain'].iloc[inds]
        tdict[patient] = time.values
        paindict[patient] = pain.values
    return tdict, paindict

def window_data():
    tdict, paindict = make_dicts()
    window_df = pd.DataFrame(columns=['Group', 'Patient', 'Time', 'Pain', 't0'])
    group = 0
    for key in tdict.keys():
        t = tdict[key]
        pain = paindict[key]
        for tmax in np.arange(wlen, np.ceil(t[-1]/wlen)*wlen, wlen):
            tmin = tmax-wlen
            inds = np.where(np.logical_and(t>tmin, t<tmax))[0]
            datadict = {
                'Group':[group for _ in inds],
                'Patient':[key for _ in inds],
                'Time':t[inds]-tmin,
                'Pain':pain[inds],
                't0':tmin
                }
            if len(inds) > wcutoff:
                temp_df = pd.DataFrame(data=datadict, columns=['Group', 'Patient', 'Time', 'Pain', 't0'])
                window_df = pd.concat([window_df, temp_df], axis='index')
                group += 1
    return window_df

def patient_number(window_df, patient_list):
    groups = int(np.max(window_df['Group'].values)+1)
    patient_list = np.zeros((groups,))
    ptnum = {}
    for j, patient in enumerate(sorted(patient_list)):
        ptnum[patient] = j
    for a, g in window_df.groupby('Group'):
         patient_list[a] = ptnum[g['Patient'][0]]
    return patient_list

def synthetic_paindata(cluster_means=[2, 8], noise=1, clustersize=100):
    pain_df = pd.DataFrame(columns=['Group', 'Patient', 'Time', 'Pain'])
    group = 0
    for i, mean in enumerate(cluster_means):
        for j in range(clustersize):
            key = f'c{i}_{j}'
            samples = int(np.round(np.random.uniform(low=5, high=21)))
            time_data = np.sort(np.random.uniform(size=(samples,))*14)
            pain_data = np.random.normal(loc=mean, scale=noise, size=(samples,))
            pain_data[pain_data < 0] = 0
            pain_data[pain_data > 10] = 10
            datadict = {
                'Group':[group for _ in range(samples)],
                'Patient':[key for _ in range(samples)],
                'Time':time_data,
                'Pain':pain_data,
                }
            temp_df = pd.DataFrame(data=datadict, columns=['Group', 'Patient', 'Time', 'Pain'])
            pain_df = pd.concat([pain_df, temp_df], axis='index')
            group += 1
    return pain_df
