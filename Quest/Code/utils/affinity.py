import numpy as np
import matplotlib.pyplot as plt
import matplotlib.transforms as mtransforms
from matplotlib.patches import FancyBboxPatch

def hausdorff(row_i, row_j):
    return (10-np.max(np.abs(row_i-row_j)))/10

def integral(row_i, row_j):
    mean_difference = np.sum(np.abs(row_i-row_j))/len(row_i)
    return (10-mean_difference)/10

def cosine(row_i, row_j):
    similarity = np.dot(row_i, row_j)/(np.sqrt(np.dot(row_i, row_i))*np.sqrt(np.dot(row_j, row_j)))
    if not np.isinf(similarity):
        return similarity
    else:
        return 0

def euclidean(row_i, row_j):
    difference = row_i-row_j
    norm = np.sqrt(np.dot(difference, difference))
    return norm

def affinity(data, similarity):
    Nsamples = len(data[:, 0])
    affmatrix = np.zeros((Nsamples, Nsamples))
    for i in range(Nsamples):
        for j in range(i, Nsamples):
            row_i = data[i, :]
            row_j = data[j, :]
            affinity_ij = similarity(row_i, row_j)
            affmatrix[i, j] = affinity_ij
            affmatrix[j, i] = affinity_ij
    affmatrix[affmatrix<0] = 0
    affmatrix[affmatrix>1] = 1
    return affmatrix

def euclidean_affinity(data):
    Nsamples = data.shape[0]
    affmatrix = np.zeros((Nsamples, Nsamples))
    for i in range(Nsamples):
        for j in range(i, Nsamples):
            row_i = data[i, :]
            row_j = data[j, :]
            affinity_ij = euclidean(row_i, row_j)
            affmatrix[i, j] = affinity_ij
            affmatrix[j, i] = affinity_ij
    affmatrix -= np.min(affmatrix.ravel())
    affmatrix /= np.max(affmatrix.ravel())
    affmatrix = 1-affmatrix
    return affmatrix

def add_fancy_patch_around(ax, bb, **kwargs):
    fancy = FancyBboxPatch((bb.xmin, bb.ymin), bb.width, bb.height,
                           fc=(1, 1, 1, 0), ec=(1, 0, 1, 1),
                           **kwargs)
    ax.add_patch(fancy)
    return fancy

def community_boxes(communities, axis):
    tot_len = 0
    for c in communities:
        bb = mtransforms.Bbox([[tot_len, tot_len], [tot_len+len(c), tot_len+len(c)]])
        fancy = add_fancy_patch_around(axis, bb, boxstyle="round,pad=0.1")
        tot_len += len(c)