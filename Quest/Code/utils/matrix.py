import numpy as np
from sklearn import cluster
import matplotlib.transforms as mtransforms
from matplotlib.patches import FancyBboxPatch

##### Functions used for assessing the co-occurrence matrix
def adj_from_labels(all_labels):
    num_samples = all_labels.shape[1]
    num_trials = all_labels.shape[0]
    adjacency = np.ones((num_samples, num_samples))
    for i in range(num_samples):
        for j in range(i+1, num_samples):
            weight = np.sum(all_labels[:, i]==all_labels[:, j])/num_trials
            adjacency[i, j] = weight
            adjacency[j, i] = weight
    return adjacency

def matrix_kmeans(adjacency, n_clusters=2):
    kmeans = cluster.KMeans(n_clusters=n_clusters).fit(adjacency)
    labels = kmeans.labels_
    communities = [list(np.argwhere(labels==j).ravel()) for j in np.unique(labels)]
    return communities

def reorder_matrix(adjacency, communities):
    size = adjacency.shape[0]
    reindex = []
    for com in communities:
        reindex += com
    reordered = np.ones((size, size))
    for i in range(size):
        for j in range(i+1, size):
            m = reindex[i]
            n = reindex[j]
            reordered[i, j] = adjacency[m, n]
            reordered[j, i] = adjacency[m, n]
    return reordered

def blockdiag(communities):
    comlens = [len(com) for com in communities]
    out = np.zeros((np.sum(comlens), np.sum(comlens)))
    start = 0
    for length in comlens:
        out[start:start+length, start:start+length] = 1
        start += length
    return out

def all_errors(adjacency, communities, norm=1):
    reordered = reorder_matrix(adjacency, communities)
    ideal = blockdiag(communities)
    return (np.abs(ideal-reordered)**norm)**(1/norm)

def modularity(adjacency, communities):
    k = np.sum(adjacency, axis=-1)
    m = np.sum(k)/2
    Q = 0
    for com in communities:
        for c in com:
            for d in com:
                B = adjacency[c, d]-k[c]*k[d]/(2*m)
                Q += B/(2*m)
    return Q

def relabel(data, labels):
    labels = labels.astype(int)
    N = int(np.max(labels)+1)
    midlist = np.zeros((N,))
    for c in range(N):
        clist = np.where(labels == c)[0]
        midlist[c] = np.mean(data[clist, 0].ravel())
    pairs = list(enumerate(midlist))
    sortlabels = sorted(pairs, key=lambda x: x[1])
    relabels = np.zeros(labels.shape)
    for new_l, (l, _) in enumerate(sortlabels):
        relabels = np.where(labels==l, new_l, relabels)
    return relabels

def communities(labels):
    coms = []
    for c in np.unique(labels):
        coms.append(list(np.where(labels==c)[0]))
    return coms

def add_fancy_patch_around(ax, bb, **kwargs):
    fancy = FancyBboxPatch((bb.xmin, bb.ymin), bb.width, bb.height,
                           fc=(1, 1, 1, 0), ec=(1, 0, 1, 1),
                           **kwargs)
    ax.add_patch(fancy)
    return fancy

def community_boxes(communities, axis):
    tot_len = 0
    for c in communities:
        bb = mtransforms.Bbox([[tot_len, tot_len], [tot_len+len(c), tot_len+len(c)]])
        fancy = add_fancy_patch_around(axis, bb, boxstyle="round,pad=0.1")
        tot_len += len(c)