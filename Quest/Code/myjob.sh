#!/bin/bash
#SBATCH -A b1020

#SBATCH -p b1020
#SBATCH -t 01:00:00
#SBATCH -n 15
#SBATCH --mem=6G

#SBATCH --mail-user=Gary.Nave@northwestern.edu
#SBATCH --mail-type=BEGIN,END,FAIL,REQUEUE

#SBATCH --output=testout.txt
#SBATCH --error=testerr.txt

#SBATCH --job-name="test"

module purge
export PATH=:/projects/b1020/gary/pain/Code/
module load python/anaconda3.6
python repeatedUMAPclustering.py synth interp_l
