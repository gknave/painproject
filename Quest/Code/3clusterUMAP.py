import argparse
import multiprocessing as mp
import numpy as np
from scipy import stats
# import matplotlib.pyplot as plt
from sklearn import cluster
import umap
import os
import timeit
import itertools
from scipy import io
import tqdm
from utils import group_windowed, relabel, centered_hist_windowed, spline_windowed

parser = argparse.ArgumentParser(description = "Generate three clusters of sample data, separated on the first axis by a distance of 1")
parser.add_argument('std', metavar='stdev', 
        type=float, nargs=1, default=1.0,
        help='Standard deviation of the distribution')
parser.add_argument('dim', metavar='dim', 
        type=int, nargs=1, default = 3,
        help='Dimension of the returned distribution')

args = parser.parse_args()
std = args.std[0]
dim = args.dim[0]

datapath = '../Data/'

## Three clusters
# threeclusterdata = np.vstack([
    # np.random.normal(loc=1.25, scale=1.0, size=(100, 10)),
    # np.random.normal(loc=0, scale=1.0, size=(100, 10)),
    # np.random.normal(loc=-1.25, scale=1.0, size=(100, 10))
    # ])
def threecluster_xaxis(offset=1, std=1, dimension=2):
    base = np.zeros((300, dimension))
    base[:100, 0] = offset
    base[-100:, 0] = -offset
    noise = np.random.normal(loc=0, scale=std, size=(300, dimension))
    return base + noise
# Uniform random
# purerandom = np.random.uniform(size=(500, 10))
## Normal random
# normalrandom = np.random.normal(size=(500, 10))
## Evenly spaced data
# def evenlyspaced(length, dim):
    # ar = np.linspace(0, 1, length)
    # mesh = np.meshgrid(*[ar for k in range(dim)])
    # data = np.array([m.flatten() for m in mesh]).T
    # return data
## Windowed with bulk properties
# windowed = group_windowed()
# realdata = windowed[:, :-1]

## Windowed with centered histogram
# cent_hist = centered_hist_windowed()
# histdata = cent_hist[:, :-1]

## Windowed with nearest-interpolated spline
# spline_data = spline_windowed()

data = threecluster_xaxis(offset=1, std=std, dimension=dim)
outname = f'{dim}d_3cluster_std{std:1.2f}.mat'
print(outname)

clusters=[2,3,4,5]

def UMAPconsistency(x, data=data, clusters=clusters,  num_trials=100):
    min_dist = x[0]
    n_neighbors = x[1]
    num_samples = len(data[:, 0])
    all_labels = np.zeros((len(clusters), num_trials, num_samples))
    all_umaps = np.zeros((2, num_trials, num_samples))
    for trial in range(num_trials):
        trialdata = data+np.random.random(size=data.shape)*0.1
        #UMAP
        umapprocess = umap.UMAP(min_dist=min_dist, n_neighbors=n_neighbors)
        u = umapprocess.fit_transform(trialdata)
        u = u/np.std(u)
        all_umaps[:, trial, :] = u.T
        #Spectral clustering
        for k, n_clusters in enumerate(clusters):
            clustering = cluster.SpectralClustering(n_clusters=n_clusters).fit(u)
            all_labels[k, trial, :] = relabel(data, clustering.labels_)
    counts = np.zeros((len(clusters), num_samples))
    for j in range(len(clusters)):
        for k in range(num_samples):
            counts[j, k] = np.sum(all_labels[j, :, k]==stats.mode(all_labels[j, :, k]))/num_trials
    return (all_umaps, all_labels, [np.sum(counts[j, :])/num_samples for j in range(len(clusters))])

if __name__ == '__main__':
    with mp.Pool(processes=8) as pool:
        dists = [0.0, 0.01, 0.05, 0.1, 0.5, 1]
        neighbors = [5, 15, 30, 50, 100]
        test_cases = itertools.product(dists, neighbors)
        output = np.zeros((len(clusters), len(dists)*len(neighbors),))
        out_details = []
        for i, res in tqdm.tqdm(enumerate(pool.imap(UMAPconsistency, test_cases)), total=len(dists)*len(neighbors)):
            out_details.append({'i':i, 'umap':res[0], 'labels':res[1]})
            output[:, i] = res[2]
        out = output.reshape((len(clusters), len(dists), len(neighbors)))
        io.savemat(datapath+outname, {'out_details':out_details, 'data':data, 'output':out})