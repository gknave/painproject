#!/usr/bin/perl

print `cd /projects/b1020/gary/pain/Code`;
print `cat ./starterfile.sh > ./myjob.sh`;
print `echo "export PATH=$PATH:/projects/b1020/gary/pain/Code/" >> ./myjob.sh`;
print `echo "module load python/anaconda3.6" >> ./myjob.sh`;
print `echo "python repeatedClustering.py" >> ./myjob.sh`;
print `sbatch myjob.sh`;
