#!/usr/bin/perl

print `cd /projects/b1020/gary/pain/Code`;

@ds = ("real","synth");
@tf = ("bulk","hist","interp_n","interp_l");

for $d (@ds) {
    for $t (@tf) {
        print `cat ./starterfile.sh > ./myjob.sh`;
        print `echo "export PATH=$PATH:/projects/b1020/gary/pain/Code/" >> ./myjob.sh`;
        print `echo "module load python/anaconda3.6" >> ./myjob.sh`;
        print `echo "python repeatedUMAPclustering.py $d $t" >> ./myjob.sh`;
        print `sbatch myjob.sh`;
    }
}

